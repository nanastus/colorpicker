// src/routes.js
import { Router } from "express";
import { hex_to_rgb, rgb_to_hex } from "./converter.js";
const routes = Router();

// Endpoint GET '{{api_url}}/'
routes.get('/', (req, res) => res.status(200).send("Welcome!"));


// Endpoint GET '{{api_url}}/rgb-to-hex?red=255&green=133&blue=0'
routes.get('/rgb-to-hex', (req, res) => {
    // collect query parameters "red", "green", and "blue"
    const RED = parseFloat(req.query.red);
    const GREEN = parseFloat(req.query.green);
    const BLUE = parseFloat(req.query.blue);
    let HEX = "";
    // sanitize and/or cobert data
    if(RED >= 0 && RED <= 255 &&
        GREEN >= 0 && GREEN <= 255 &&
        BLUE >=0 && BLUE <= 255){
           HEX =  rgb_to_hex(RED, GREEN, BLUE);
        } else {
            res.status(400).send("Invalid input!")
        }
    // conbert RGB values to HEX value
    // respond with HEX value
    res.status(200).send(HEX);
});

// Endpoint GET '{{api_url}}/hex-to-rgb'
routes.get('/hex-to-rgb', (req, res) => {
    const HEX = req.query.hex;
    let RGB = [];
    if(HEX.length == 6){
        RGB = hex_to_rgb(HEX)
    } else {
        res.status(400).send("Invalid input!")
    }
    res.status(200).send(RGB)
});

export default routes;