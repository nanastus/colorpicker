// import express from 'express';
// import routes from './routes.js';
// const app = express();
// app.use('/api/v1', routes);
// const api_url = "http://localhost:3000"
// app.listen(3000, () => console.log(
//     `Server listening at ${api_url}`
// ));

import express from "express";
import ViteExpress from "vite-express";
import routes from "./routes.js";

const app = express();

app.use('/api', routes);
const api_url = "http://localhost:3000";

app.get("/hello", (req, res) => {
  res.send("Hello Vite!");
});

ViteExpress.listen(app, 3000, () =>
  console.log(`Server is listening on ${api_url}`),
);
