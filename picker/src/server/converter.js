// src/converter.js

/**
 * Padding hex component if necessary.
 * Hex component representation requires.
 * two hexadecimal characters.
 * @param {string} comp
 * @returns {string} two hexadecimal characters.
 */
const pad = (comp) => {
    let padded = comp.length == 2 ? comp : "0" +comp;
    return padded;
}
/**
 * RGB-to-HEX conversion.
 * @param {number} r RED 0-255 
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g., #00ff00 (green)
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
    
};
/**
 * HEX-to-RGB conversion.
 * @param {string} string HEX without #, 000000-ffffff
 * @returns {array} values for r, g and b
 */
export const hex_to_rgb = (string) => {
    const redPart = string.slice(0, 2);
    const greenPart = string.slice(2,4);
    const bluePart = string.slice(4,6);
    const red = parseInt(redPart, 16);
    const green = parseInt(greenPart, 16);
    const blue = parseInt(bluePart, 16);
    const rgb = [red, green, blue];
    return rgb;
}