import '../client/style.css'

document.querySelector('#app').innerHTML = `
  <div id="container">
    <h1>Color picker</h1>
    <div id="picker-top">
      <div id="sliders">
        <div id="slider" class="red">
          <input type="range" min="0" max="255" class="slider-input">
          <div class="slider-value">0</div>
        </div>
        <div id="slider" class="green">
          <input type="range" min="0" max="255" class="slider-input">
          <div class="slider-value">0</div>
        </div>
        <div id="slider" class="blue">
          <input type="range" min="0" max="255" class="slider-input">
          <div class="slider-value">0</div>
        </div>
      </div>
      <div id="colorbox">
        <div id="colorarea"></div>
      </div>
    </div>
    <div id="picker-bottom">
      <input type="text" id="hex" class="input">
      <input type="text" id="rgb" class="input">
    </div>
  </div>
`
document.addEventListener("DOMContentLoaded", function() {
  const sliders = document.querySelectorAll(".slider-input");
  const preview = document.querySelector("#colorarea");
  const hexfield = document.querySelector("#hex");

  sliders.forEach(slider => {
    slider.addEventListener("input", updateColor);
    updateColor();
  });

  hexfield.addEventListener("keydown", function(event) {
    if(event.keyCode === 13) {
      updateHex();
    }
  });

  function updateColor() {
    const red = document.querySelector(".red .slider-input").value;
    const green = document.querySelector(".green .slider-input").value;
    const blue = document.querySelector(".blue .slider-input").value;
  
    const color = `rgb(${red}, ${green}, ${blue})`;
    preview.style.backgroundColor = color;

    document.querySelector(".red .slider-value").textContent = red;
    document.querySelector(".green .slider-value").textContent = green;
    document.querySelector(".blue .slider-value").textContent = blue;

    document.querySelector("#rgb").value = color;

    const api_url = "http://localhost:3000/api"
    const requestUrl = `${api_url}/rgb-to-hex?red=${red}&green=${green}&blue=${blue}`;

    fetch(requestUrl)
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.text();
    })
    .then(hex => {
      // Assuming the API returns the hex color code in the 'hex' property
      document.querySelector("#hex").value = hex;
    })
    .catch(error => {
      console.error('There was a problem with the fetch operation:', error);
    });
  }

  function updateHex() {
    const hexvalue = document.querySelector("#hex").value;
    let hex = "";
    if(hexvalue.length >= 7) {
      hex = hexvalue.slice(1, 8);
    }
    const api_url = "http://localhost:3000/api"
    const requestUrl = `${api_url}/hex-to-rgb?hex=${hex}`;

    fetch(requestUrl)
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      const red = data[0];
      const green = data[1];
      const blue = data[2];
      const color = `rgb(${red}, ${green}, ${blue})`;
      preview.style.backgroundColor = color;
      document.querySelector("#rgb").value = color;
      document.querySelector(".red .slider-value").textContent = red;
      document.querySelector(".green .slider-value").textContent = green;
      document.querySelector(".blue .slider-value").textContent = blue;
    })
    .catch(error => {
      console.error('There was a problem with the fetch operation:', error);
    });
  }
});
