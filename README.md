# H1 Integration testing excercise with frontend
---

This is a school project to practice integration testing.

Frontend has a simple UI with red, green and blue sliders and a preview box.
It also has fields for hex and rgb values

![Screenshot](/picker/images/picker.png)

You can get the HEX code by selecting a color with the slicers.
You can also write the HEX code to the input field (enter) and get the RGB value.

Backend has 3 endpoints:

get / for testing 

get /rgb-to-hex for changing rgb to hex
- rgb needs to be given
  - red=
  - green=
  - blue=
get /hex-to-rgb for changing hex to rgb
- hex needs to be given without #
